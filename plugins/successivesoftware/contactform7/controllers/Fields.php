<?php namespace Successivesoftware\Contactform7\Controllers;

use BackendMenu;
use Backend\Classes\Controller;

/**
 * Fields Back-end Controller
 */
class Fields extends Controller
{
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController'
    ];

    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';

    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('Successivesoftware.Contactform7', 'contactform7', 'fields');
    }
}
