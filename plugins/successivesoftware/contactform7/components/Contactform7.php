<?php namespace SuccessiveSoftware\Contactform7\Components;

use Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\URL;
use Request;
use Cms\Classes\ComponentBase;
use Successivesoftware\Contactform7\Models\Contact;
use System\Classes\ApplicationException;
use Mail;

class Contactform7 extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'Form Creator',
            'description' => 'Generate Form.'
        ];
    }

    public function defineProperties()
    {
        return [
            'contactForm' => [
                'title'             => 'Select',
                'description'       => 'Select Form',
                'type'              => 'dropdown',
                'placeholder'       => 'Select Form',
            ]
        ];
    }
    public function getContactFormOptions()
    {
        return Contact::lists('form_name', 'id');
    }

    public function onRun()
    {
        if($_REQUEST) {
            if(is_array(post('contact'))) {
                foreach (post('contact') as $key => $field) {
                    if (is_array($field)) {
                        $contact[$key] = implode(', ', $field);
                    } else {
                        $contact[$key] = $field;
                    }
                }

                $data = [
                    "mailto" => post('mailto'),
                    "mailfrom" => post('mailfrom'),
                    "mailsubject" => post('mailsubject'),
                    "mailsender" => post('mailsender'),
                    "mail_success" => post('mail_success'),
                    "mail_failed" => post('mail_failed'),
                    "fields" => $contact,
                ];
                try {
                    \Mail::send('successivesoftware.contactform7::mail.sent', $data, function ($message) use ($data) {
                        $message->to($data['mailto'], $data['mailsubject']);
                    });
                    $this->page['result'] = $data['mail_success'];
                } catch (\Exception $e) {
                    $this->page['result'] = $data['mail_failed'];
                }
            }else{
                $this->page['result'] = 'Invalid data provided, Please provide fields to form.';
            }
        }
        $this->page['formInfo'] = json_decode($this->info());
    }

	public function info()
	{
	    $formId = $this->property('contactForm');
	    $formData = Contact::where('id',$formId)->first();
        $fields = DB::table('successivesoftware_contactform7_fields')
            ->join('successivesoftware_contactform7_field_entity','successivesoftware_contactform7_fields.id','=','successivesoftware_contactform7_field_entity.field_id')
            ->where('successivesoftware_contactform7_field_entity.entity_id','=',$formData['id'])
            ->get();

        foreach ($fields as $field){
            if($field->options != ""){
                $field->options = explode(':',$field->options);
            }
        }

        $data = [
		    'contactForm' => $formData['form_name'],
            'mailto'=>$formData['mailto'],
            'mailfrom'=>$formData['mailfrom'],
            'mailsubject'=>$formData['mailsubject'],
            'mailsender'=>$formData['mailsender'],
            'mail_success'=>$formData['mail_success'],
            'mail_failed'=>$formData['mail_failed'],
            'fields' => $fields
        ];


		return json_encode($data);
	}

}

?>
