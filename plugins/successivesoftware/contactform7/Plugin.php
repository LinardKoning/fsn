<?php namespace SuccessiveSoftware\Contactform7;

use System\Classes\PluginBase;
use Backend;
class Plugin extends PluginBase
{

    public function pluginDetails()
    {
        return [
            'name'        => 'Form Creator',
            'description' => 'Create customized frontend forms',
            'author'      => 'SuccessiveSoftware',
            'icon'        => 'icon-sun-o'
        ];
    }
    
    public function registerComponents()
    {
        return [
           '\SuccessiveSoftware\Contactform7\Components\Contactform7' => 'ContactForm',
        ];
    }
    

    public function registerNavigation()
    {   
        return [
            'contactform7' => [
                'label'       => 'Forms',
                'icon'        => 'icon-envelope',
                'url'         => Backend::url('successiveSoftware/contactform7/contacts'),
                'permissions' => ['contactform7.*'],
                'order'       => 500,
                'sideMenu' => [
                    'forms' => [
                        'label'       => 'Contact Forms',
                        'icon'        => 'icon-file-text-o',
                        'url'         => Backend::url('successiveSoftware/contactform7/contacts'),
                        'permissions' => ['contactform7.manage_forms'],
                    ],
                    'add' => [
                        'label'       => 'Add New',
                        'icon'        => 'icon-table',
                        'url'         => Backend::url('successiveSoftware/contactform7/addcontact'),
                        'permissions' => ['contactform7.access_forms'],
                    ]
                ]
            ]
        ];
    }
    

    
}
