<?php namespace Successivesoftware\Contactform7\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateFieldsTable extends Migration
{
    public function up()
    {
        Schema::create('successivesoftware_contactform7_fields', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('field_type');
            $table->string('label');
            $table->string('field_name');
            $table->string('default_value');
            $table->integer('is_required');
            $table->string('custom_attribute');
            $table->string('placeholder');
            $table->string('classes');
            $table->integer('is_visible');
            $table->longText('options');
            $table->timestamps();
        });
        Schema::create('successivesoftware_contactform7_field_entity', function($table) {
            $table->engine = 'InnoDB';
            $table->unsignedInteger('field_id')->index('field_id_idx');
            $table->unsignedInteger('entity_id')->index('entity_id_idx');
            $table->string('entity_type')->index('entity_type_idx');
            $table->primary(['field_id', 'entity_id', 'entity_type'], 'field_entity_pk');
        });
    }

    public function down()
    {
        Schema::dropIfExists('successivesoftware_contactform7_fields');
        Schema::dropIfExists('successivesoftware_contactform7_field_entity');
    }
}
