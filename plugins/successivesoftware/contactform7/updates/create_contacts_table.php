<?php namespace Successivesoftware\Contactform7\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateContactsTable extends Migration
{
    public function up()
    {
        Schema::create('successivesoftware_contactform7_contacts', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('form_name')->nullable();
            $table->longText('form_content')->nullable();
            $table->string('mailto')->nullable();
            $table->string('mailfrom')->nullable();
            $table->string('mailsubject')->nullable();
            $table->string('mailsender')->nullable();
            $table->string('mailbody')->nullable();
            $table->string('mail_success')->nullable();
            $table->string('mail_failed')->nullable();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('successivesoftware_contactform7_contacts');
    }
}
