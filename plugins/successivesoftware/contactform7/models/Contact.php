<?php namespace Successivesoftware\Contactform7\Models;

use Model;

/**
 * Contact Model
 */
class Contact extends Model
{
    /**
     * @var string The database table used by the model.
     */
    public $table = 'successivesoftware_contactform7_contacts';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = ['form_name','mailto','mailfrom','mailsubject','mailsender','mail_success','mail_failed'];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];

    /**
     * @var array Relations
     */

    public $morphToMany = [
        'fields' => [
            'Successivesoftware\Contactform7\Models\Field',
            'name' => 'entity',
            'table' => 'successivesoftware_contactform7_field_entity'
        ],
    ];
}
