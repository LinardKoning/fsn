<?php namespace Successivesoftware\Contactform7\Models;

use Model;

/**
 * Field Model
 */
class Field extends Model
{
    /**
     * @var string The database table used by the model.
     */
    public $table = 'successivesoftware_contactform7_fields';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = ['field_type','label','field_name','default_value','is_required','custom_attribute','placeholder','classes','is_visible','options'];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];


}
