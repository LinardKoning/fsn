#Form Creator Plugin

Description

Form Creator plugin allows you to easily create any type of customized forms for frontend.

Features
+ Fast install and flexible setup
+ Create any type of customized forms for frontend from your backend dashboard
+ Easily Customizable
+ Create custom fields
+ Quick settings & mail configurations
+ Responsive mail template


Documentation & Usage - Backend

After installation of plugin you will able to create cutomized forms from the "Forms" menu position in the backend.

Plugin will register form creator component. So you need to add the component of the respective page you will create. After inserting the component you will enter the form name and select the form from the dropdown option (that's will show the list of form's generating in the backend).
